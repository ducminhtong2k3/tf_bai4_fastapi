from sqlalchemy import Column, Integer, String, DateTime, func

from app.database.database_config import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    password = Column(String, index=True)


class Article(Base):
    __tablename__ = 'articles'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    context = Column(String, index=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
