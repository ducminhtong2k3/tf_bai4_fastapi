from pydantic import BaseModel


class ArticleBase(BaseModel):
    title: str
    context: str
    created_at: str

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    username: str
    password: str

    class Config:
        orm_mode = True
