from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# create database URL
SQLALCHEMY_DATABASE_URL = "sqlite:///./database.db"

# create engine
engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)

# create database session class
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# create Base class
Base = declarative_base()
