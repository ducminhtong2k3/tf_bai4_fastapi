from fastapi import Depends, HTTPException, APIRouter
from sqlalchemy.orm import Session

from app.database.get_db import get_db
from app.models_and_schemas import schemas
from app.lib import user_services, auth_tokens

router = APIRouter()


@router.post('/signup', response_model=schemas.UserBase)
def signup(user: schemas.UserBase, db: Session = Depends(get_db)):
    db_user = user_services.get_user(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail='Account is registered')
    return user_services.signup(db, user=user)


@router.post('/login')
def login(user_login: schemas.UserBase, db: Session = Depends(get_db)):
    if user_services.verify(db, user=user_login):
        token = auth_tokens.generate_token(username=user_login.username)
        return {'token': token}
    else:
        raise HTTPException(status_code=404, detail='Login Failed')
