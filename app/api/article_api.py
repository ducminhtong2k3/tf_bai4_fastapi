from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session

from app.models_and_schemas import schemas
from app.database.get_db import get_db
from app.lib.auth_tokens import validate_token
from app.lib import article_services

router = APIRouter()


@router.get('/')
def lists(db: Session = Depends(get_db)):
    return article_services.list_article(db)


@router.get('/page')
def paging(page: int, size: int, db: Session = Depends(get_db)):
    return article_services.paginate_article(db, page=page, size=size)


@router.post('/create', dependencies=[Depends(validate_token)])
def create(article: schemas.ArticleBase, db: Session = Depends(get_db)):
    return article_services.create_article(db, article=article)


@router.put('/update/{article_id}', dependencies=[Depends(validate_token)])
def update(article: schemas.ArticleBase, article_id: int, db: Session = Depends(get_db)):
    return article_services.update_article(db, article_id=article_id, article_change=article)


@router.get('/delete/{article_id}', dependencies=[Depends(validate_token)])
def delete(article_id: int, db: Session = Depends(get_db)):
    return article_services.delete_article(db, article_id=article_id)
