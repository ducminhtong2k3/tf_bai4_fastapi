from sqlalchemy.orm import Session
from app.models_and_schemas import models, schemas


def create_article(db: Session, article: schemas.ArticleBase):
    new_article = models.Article(
        title=article.title,
        context=article.context,
    )
    db.add(new_article)
    db.commit()
    db.refresh(new_article)
    return new_article


def list_article(db: Session):
    return db.query(models.Article).all()


def paginate_article(db: Session, page: int, size: int):
    return db.query(models.Article).offset(page).limit(size).all()


def update_article(db: Session, article_id: int, article_change: schemas.ArticleBase):
    db_article = db.query(models.Article).filter(models.Article.id == article_id).first()
    db_article.title = article_change.title
    db_article.context = article_change.context
    db.add(db_article)
    db.commit()
    db.refresh(db_article)
    return db_article


def delete_article(db: Session, article_id: int):
    db_article = db.query(models.Article).filter(models.Article.id == article_id).first()
    db.delete(db_article)
    db.commit()
    return {'message': 'Delete Successfully'}
