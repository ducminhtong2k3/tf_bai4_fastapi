import jwt
from fastapi.security import HTTPBearer
from fastapi import Depends, HTTPException

from datetime import datetime, timedelta
from pydantic import ValidationError

from instance.config import settings


reusable_oauth2 = HTTPBearer(
    scheme_name='Authorization'
)


def generate_token(username):
    expire = datetime.utcnow() + timedelta(minutes=60)
    to_encode = {"exp": expire, "username": username}
    token = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.SECURITY_ALGORITHM)
    return token


def validate_token(credentials=Depends(reusable_oauth2)):
    try:
        payload = jwt.decode(credentials.credentials, settings.SECRET_KEY, algorithms=settings.SECURITY_ALGORITHM)
        expired = payload.get('exp')
        if datetime.utcnow() > datetime.utcfromtimestamp(expired):
            raise HTTPException(status_code=403, detail='Token expired')
        return payload.get('username')
    except(jwt.PyJWTError, ValidationError):
        raise HTTPException(status_code=403, detail='Token is invalid or Token is expired.'
                                                    'Please login to get token')
