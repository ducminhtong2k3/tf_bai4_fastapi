from sqlalchemy.orm import Session
from app.models_and_schemas import models, schemas
from werkzeug.security import check_password_hash, generate_password_hash


def get_user(db: Session, username: str):
    return db.query(models.User).filter(models.User.username == username).first()


def signup(db: Session, user: schemas.UserBase):
    new_user = models.User(
        username=user.username,
        password=generate_password_hash(user.password)
    )
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return {'message': 'Signup Successfully'}


def verify(db: Session, user: schemas.UserBase):
    db_user = db.query(models.User).filter(models.User.username == user.username).first()
    if check_password_hash(db_user.password, user.password):
        return True
    else:
        return False
    