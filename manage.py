import uvicorn
from fastapi import FastAPI

from app.models_and_schemas import models
from app.database.database_config import engine
from app.api import article_api, user_api

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(article_api.router)
app.include_router(user_api.router)

if __name__ == '__main__':
    uvicorn.run('manage:app', host='0.0.0.0', reload=True)
